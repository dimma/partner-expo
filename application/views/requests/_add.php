<!-- Large modal -->
<div id="modal-add-request" class="modal fade" role="dialog" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Добавление заявки</h4>
			</div>
			<div class="modal-body">
				<?=Form::open('/requests/add', array('id' => 'add-request-form', 'class' => 'ajax-form'))?>
				<div class="panel-body no-padding-hr">
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<label class="col-sm-4 control-label">Клиент:</label>
							<div class="col-sm-8">
								<input type="text" name="name" class="form-control" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<label class="col-sm-4 control-label">Контактное лицо:</label>
							<div class="col-sm-8">
								<input type="text" name="contact_person" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<label class="col-sm-4 control-label">Телефон:</label>
							<div class="col-sm-6">
								<input type="text" name="phone" placeholder="+7 999 999-99-99" class="form-control mask-phone">
							</div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<label class="col-sm-4 control-label">E-mail:</label>
							<div class="col-sm-6">
								<input type="text" name="email" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<label class="col-sm-4 control-label">Примечание:</label>
							<div class="col-sm-8">
								<textarea name="comment" class="form-control textarea-transition"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
						<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-8">
								<button class="btn btn-primary btn-flat btn-lg">Создать заявку</button>
							</div>
						</div>
					</div>
				</div>
				<?=Form::close()?>
			</div>
		</div> <!-- / .modal-content -->
	</div> <!-- / .modal-dialog -->
</div> <!-- / .modal -->
<!-- / Large modal -->