<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'layout';

	public function before()
	{
		parent::before();

		# Смотрим, залогинен ли юзер
		$auth = Auth::instance();

		if($this->request->controller() !== 'Recovery')
		{
			if($auth->logged_in())
			{
				$this->user = Auth::instance()->get_user();
			}
			# Если нет, то просим его авторизоваться
			else
			{
				if($this->request->controller() !== 'auth' && $this->request->action() !== 'login')
				{
					Controller::redirect('/login');
				}
			}
		}
		else
		{
			$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
		}

		# Параметры шаблона
		if($this->auto_render)
		{
			//$detect = new MobileDetect();

			$this->template->title				= '';
			$this->template->parent_title		= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= 'SiteExpo.ru';
			$this->template->styles				= array();
			$this->template->scripts			= array();
			$this->template->controller			= UTF8::strtolower($this->request->controller());
			$this->template->action				= $this->request->action();
			$this->template->main_page			= ($this->request->controller() == 'main' && $this->request->action() == 'index') ? TRUE : FALSE;
			$this->template->content			= '';
			$this->template->menu				= Kohana::$config->load('menu');
			$this->template->user_title			= ($auth->logged_in() && isset($this->user)) ? $this->user->title : FALSE;
			$this->template->user				= ($auth->logged_in() && isset($this->user)) ? $this->user : FALSE;
			$this->template->breadcrumbs        = '';
			$this->template->icon               = '';
			//$this->template->mobile             = ($detect->isMobile()) ? TRUE : FALSE;
			$this->template->avatar             = '';
			$this->template->rights				= array();
			$this->template->digits				= array();
			$this->template->notifications		= FALSE;
			$this->template->course				= array();
			$this->template->header_content		= '';

			# Rights
			if($this->template->user)
			{
				foreach(ORM::factory('Role')->find_all() as $role)
				{
					$this->template->rights[$role->name] = FALSE;
				}

				foreach($this->user->roles->find_all() as $role)
				{
					$this->template->rights[$role->name] = TRUE;
				}

				# Course
				/*
				if($this->user->has('roles', ORM::factory('Role', array('name' => 'admin'))) || $this->user->has('roles', ORM::factory('Role', array('name' => 'manager'))))
				{
					$currency_path = "http://www.cbr.ru/scripts/XML_daily.asp";
					$xml = simplexml_load_file($currency_path);

					$this->template->course = array(
						'euro' => $xml->Valute[10]->Value,
						'usd' => $xml->Valute[9]->Value,
					);
				}
				*/

				# Notifications
				if($this->user->has('roles', ORM::factory('Role', array('name' => 'admin'))) || $this->user->has('roles', ORM::factory('Role', array('name' => 'manager'))))
				{
					//$this->template->notifications = ORM::factory('notification')->where('status','=','1')->and_where('user_id','in',array('0',$this->user->id))->find_all();
				}
			}

			$a_uri = explode('/', Request::current()->uri());

			if(count($a_uri) == 1)
			{
				$this->template->icon = (isset($this->template->menu[$a_uri[0]]) && isset($this->template->menu[$a_uri[0]]['icon'])) ? $this->template->menu[$a_uri[0]]['icon'] : '';
			}
			elseif(count($a_uri) > 1)
			{
				$this->template->icon = (isset($this->template->menu[$a_uri[0]]) && isset($this->template->menu[$a_uri[0]]['items'][$a_uri[1]]['icon'])) ? $this->template->menu[$a_uri[0]]['items'][$a_uri[1]]['icon'] : '';
			}
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			# Проверяем, есть ли скрипты или стили для конкретного раздела
			if(file_exists(DOCROOT.'/assets/css/'.$this->template->controller.'.css'))
			{
				$styles['assets/css/'.$this->template->controller.'.css'] = 'screen';
			}

			if(file_exists(DOCROOT.'/assets/js/'.$this->template->controller.'.js'))
			{
				$scripts[] = 'assets/js/'.$this->template->controller.'.js';
			}

			# Подключаем стандартные стили и скрипты
			$styles['assets/stylesheets/bootstrap.min.css'] = 'screen';
			$styles['assets/stylesheets/pixel-admin.min.css'] = 'screen';
			$styles['assets/stylesheets/widgets.min.css'] = 'screen';
			$styles['assets/stylesheets/pages.min.css'] = 'screen';
			$styles['assets/stylesheets/rtl.min.css'] = 'screen';
			$styles['assets/stylesheets/themes.min.css'] = 'screen';
			$styles['assets/stylesheets/layout.css'] = 'screen';

			$scripts[] = 'assets/javascripts/bootstrap.min.js';
			//$scripts[] = 'assets/javascripts/jquery.transit.js';
			$scripts[] = 'assets/javascripts/pixel-admin.min.js';
			//$scripts[] = 'assets/javascripts/bootstrap-datepicker.ru.js';
			$scripts[] = 'assets/javascripts/common.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>
