<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>PartnerExpo — Авторизация</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

	<!-- Pixel Admin's stylesheets -->
	<link href="/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/stylesheets/layout.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
	<script src="/assets/javascripts/ie.min.js"></script>
	<![endif]-->

	<style type="text/css">
		body.page-signin-alt {
			background: url('/assets/i/bg_section_1_bw.jpg') no-repeat center top;
			color: #fff;
		}
		#signin-form h1 { margin-top: 0; font-size: 30px; font-weight: 300 !important; font-family: Exo2; }
		h1 p span { font-weight: 300 !important; }
		#signin-form { background: none; border: none; top: 0; position: absolute; top: 50%; left: 50%; margin: -200px 0 0 -180px; }
		#btn-signup { border-radius: 0; background: none; border: 1px solid #fff; text-transform: uppercase; transition: background 0.2s ease-in; }
		#btn-signup:hover { background: #fff !important; color: #000; }
		#btn-signup:active { opacity: 0.5; background: #fff !important; color: #000; border: 1px solid #fff !important; }

		#signin-form input[type="text"], #signin-form input[type="password"] {
			padding: 10px 16px; text-align: center; color: #000 !important; border: none; border-radius: 0; opacity: 0.6; transition: opacity 0.2s ease-in;
			-webkit-box-shadow: inset 0 0 0 50px #fff;
			-webkit-text-fill-color: #000;
		}
		#signin-form input[type="text"]:focus, #signin-form input[type="password"]:focus { opacity: 0.7; }

		#signin-form input[type="text"]::-webkit-input-placeholder, #signin-form input[type="password"]::-webkit-input-placeholder {
			color: #555;
		}

		#signin-form input[type="text"]:-moz-placeholder, #signin-form input[type="password"]:-moz-placeholder { /* Firefox 18- */
			color: #555;
		}

		#signin-form input[type="text"]::-moz-placeholder, #signin-form input[type="password"]::-moz-placeholder {  /* Firefox 19+ */
			color: #555;
		}

		#signin-form input[type="text"]:-ms-input-placeholder, #signin-form input[type="password"]:-ms-input-placeholder {
			color: #555;
		}

	</style>
</head>


<!-- 1. $BODY ======================================================================================

	Body

	Classes:
	* 'theme-{THEME NAME}'
	* 'right-to-left'     - Sets text direction to right-to-left
-->
<body class="theme-default page-signin-alt">
<!-- 2. $MAIN_NAVIGATION ===========================================================================

	Main navigation
-->
<? /*
<div class="signin-header">
	<a href="#" class="logo">
		<div class="demo-logo bg-primary"><img src="assets/demo/logo-big.png" alt="" style="margin-top: -4px;"></div>&nbsp;
		<strong>Partner</strong>Expo
	</a> <!-- / .logo -->
</div> <!-- / .header -->
*/ ?>

<!-- Form -->
<?=Form::open('/login/', array('class'=>'panel','id'=>'signin-form'))?>
	<h1 class="form-header">Partner<span>Expo</span></h1>

	<div class="form-group">
		<input type="text" name="username" id="username_id" class="form-control input-lg" placeholder="Логин">
	</div> <!-- / Username -->

	<div class="form-group signin-password">
		<input type="password" name="password" id="password_id" class="form-control input-lg" placeholder="Пароль">
		<? /* <a href="#" class="forgot">Забыли пароль?</a> */ ?>
	</div> <!-- / Password -->

	<div class="form-actions">
		<input type="submit" value="Войти" class="btn btn-primary btn-block btn-lg" id="btn-signup">
	</div> <!-- / .form-actions -->
<?=Form::close()?>
<!-- / Form -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="/assets/javascripts/bootstrap.min.js"></script>
<script src="/assets/javascripts/pixel-admin.min.js"></script>

<script type="text/javascript">
	window.PixelAdmin.start([
		function () {
			$("#signin-form_id").validate({ focusInvalid: true, errorPlacement: function () {} });

			// Validate username
			$("#username_id").rules("add", {
				required: true,
				minlength: 3
			});

			// Validate password
			$("#password_id").rules("add", {
				required: true,
				minlength: 6
			});
		}
	]);
</script>

</body>
</html>
