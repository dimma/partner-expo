<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Payments extends Controller_Layout {
	# Список заявок
	public function action_index()
	{
		$data = array();

		//$data['payments'] = ORM::factory('Payment')->find_all();

		$this->template->window_title = 'Платежи | PartnerExpo';
		$this->template->title = 'Платежи';
		//$this->template->header_content = View::factory('requests/_top', $data);
		$this->template->content = View::factory('payments/index', $data);
	}
}
