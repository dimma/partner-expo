<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Clients extends Controller_Layout {
	# Список заявок
	public function action_index()
	{
		$data = array();

		$data['clients'] = ORM::factory('Client')->find_all();

		$this->template->window_title = 'Клиенты | PartnerExpo';
		$this->template->title = 'Клиенты';
		//$this->template->header_content = View::factory('requests/_top', $data);
		$this->template->content = View::factory('clients/index', $data);
	}
}
