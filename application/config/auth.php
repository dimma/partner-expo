<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => '203uwlevcjho2hevLQDQ@@3FDFS@#$$sdfUHO832FH',
	'lifetime'     => 1209600,
	'session_key'  => 'auth_user'
);