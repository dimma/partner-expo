<?php defined('SYSPATH') or die('No direct script access.');

class Model_Client extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'partner' => array('model' => 'partner', 'foreign_key' => 'partner_id'),
	);

	public function rules()
	{
		return array(
			'name' => array(array('not_empty')),
			'contact_person' => array(array('not_empty')),
			'phone' => array(array('not_empty'), array('phone', array(':value', array(11)))),
			'email' => array(array('not_empty'), array('email')),
		);
	}

	/*
	protected $_has_many = array(
		'messages' => array('model' => 'message', 'foreign_key' => 'request_id'),
	);
	*/
}
?>