<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'requests' => array(
		'url' => 'requests',
		'icon' => 'list',
		'name' => 'Заявки',
/*
		'items' => array(
			'new' => array(
				'url' => 'new',
				'icon' => 'bolt',
				'name' => 'Новые',
			),
			'confirmed' => array(
				'url' => 'confirmed',
				'icon' => 'check',
				'name' => 'Подтвержденные',
			),
			'courier' => array(
				'url' => 'courier',
				'icon' => 'truck',
				'name' => 'Курьер',
			),
			'cancelled' => array(
				'url' => 'cancelled',
				'icon' => 'minus-circle',
				'name' => 'Отмененные',
			),
			'archive' => array(
				'url' => 'archive',
				'icon' => 'archive',
				'name' => 'Архив',
			),
		)
*/
	),
	'payments' => array(
		'url' => 'payments',
		'icon' => 'rouble',
		'name' => 'Платежи',
	),
	'clients' => array(
		'url' => 'clients',
		'icon' => 'users',
		'name' => 'Клиенты',
	),
);
?>