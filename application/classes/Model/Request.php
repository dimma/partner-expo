<?php defined('SYSPATH') or die('No direct script access.');

class Model_Request extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				//array('Security::xss_clean'),
			),
		);
	}

	protected $_belongs_to = array(
		'partner' => array('model' => 'Partner', 'foreign_key' => 'partner_id'),
		'client' => array('model' => 'Client', 'foreign_key' => 'client_id'),
	);

	/*
	public function rules()
	{
		return array(
			'text' => array(array('not_empty')),
		);
	}


	protected $_has_many = array(
		'messages' => array('model' => 'message', 'foreign_key' => 'request_id'),
	);
	*/
}
?>