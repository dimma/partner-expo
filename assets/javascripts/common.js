// Scripts
// uses jquery
// ++++++++++++++++++++++++++++++++++++++++++

!function ($) {
	$(function(){
		// Datepicker
		$('#bs-datepicker-component').datepicker({
			format: 'dd.mm.yyyy',
			pickTime: false,
			dates: 'ru'
		}).on('changeDate', function(e){
			$('.datepicker').hide();
		});

		$('.mask-phone').mask('+7 999 999-99-99');

		$('select').select2();
		$('textarea').autosize();

		$('#add-request-btn').click(function(){
			$('#add-request-form input[type="text"]:eq(0)').focus();
		});

		// Ajax form
		$('.ajax-form').unbind().submit(function(){
			var self = this;

			$.post($(this).attr('action'), $(this).serialize(), function(data){
				var response = $.parseJSON(data);

				$(self).find('.has-error').removeClass('error');
				$(self).find('.help-block').hide();

				if(response.errors)
				{
					for(var n in response.errors)
					{
						$(self).find('*[name="'+n+'"]').parents('.form-group').addClass('has-error');
						$(self).find('*[name="'+n+'"]').parents('.form-group').find('.help-block').show();
					}
				}

				if(response.status)
				{
					$('.ajax-form-success').fadeIn('fast');
					setTimeout(function(){ $('.ajax-form-success').fadeOut('fast'); }, 3000);
				}

			});
			return false;
		});
	})
}(window.jQuery)