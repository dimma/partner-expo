<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Requests extends Controller_Layout {
	# Список заявок
	public function action_index()
	{
		$data = array();

		$data['requests'] = ORM::factory('Request')->find_all();
		$data['statuses'] = Kohana::$config->load('statuses');

		$this->template->window_title = 'Заявки | PartnerExpo';
		//$this->template->parent_title = 'Заявки';
		$this->template->title = 'Заявки';
		$this->template->header_content = View::factory('requests/_top', $data);
		$this->template->content = View::factory('requests/index', $data);	}
}
