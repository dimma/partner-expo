<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajaxlayout extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'ajax';

	public function before()
	{
		parent::before();

		$auth = Auth::instance();
		$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
	}

	public function after()
	{
		parent::after();
	}
}
?>