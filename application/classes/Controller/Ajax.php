<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller_Ajaxlayout {

	public function action_index()
	{

		exit;
	}

	public function action_add_request()
	{
		if($this->request->post() && $this->user)
		{
			$client = ORM::factory('client', array('email' => $this->request->post('email')));

			# Client
			if(!$client->id)
			{
				// Client
				try
				{
					$client = ORM::factory('Client')->values(array(
						'name' => $this->request->post('name'),
						'contact_person' => $this->request->post('contact_person'),
						'phone' => $this->request->post('phone'),
						'email' => $this->request->post('email'),
						'status' => '1',
						'date' => Date::formatted_time(),
						'partner_id' => $this->user->id
					))->save();
				}
				catch(ORM_Validation_Exception $e)
				{
					$json['errors'] = $e->errors();
					$json['status'] = 0;
				}
			}

			# Request
			if($client->id)
			{
				try
				{
					$request = ORM::factory('Request')->values(array(
						'client_id' => $client->id,
						'source' => '1',
						'date' => Date::formatted_time(),
						'status' => '1',
						'comment' => $this->request->post('comment')
					))->save();
					$json['status'] = 1;
				}
				catch(ORM_Validation_Exception $e)
				{
					$json['errors'] = $e->errors();
					$json['status'] = 0;
				}
			}

			echo json_encode($json);
		}

		exit;
	}
}
?>