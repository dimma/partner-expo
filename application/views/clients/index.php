<div class="solid">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Название</th>
				<th>Контактное лицо</th>
				<th>Телефон</th>
				<th>E-mail</th>
				<th class="text-center">Дата регистрации</th>
			</tr>
		</thead>
		<tbody class="notes-list" id="clients-list">
		<? foreach($clients as $client) { ?>
			<tr class="status-0">
				<td><?=$client->name?></td>
				<td><?=$client->contact_person?></td>
				<td><?=$client->phone?></td>
				<td><a href="mailto:<?=$client->email?>"><?=$client->email?></a></td>
				<td class="text-center"><?=Date::format($client->date,'d F Y')?></td>
			</tr>
		<? } ?>
		</tbody>
	</table>
</div>
