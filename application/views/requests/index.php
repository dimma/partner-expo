<div class="solid">
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="thin">&#8470;</th>
				<th>Клиент</th>
				<th>Источник</th>
				<th>Дата</th>
				<th>Статус</th>
			</tr>
		</thead>
		<tbody class="notes-list" id="requests-list">
			<? foreach($requests as $request) { ?>
				<tr class="status-0">
					<td class="thin"><?=$request->id?></td>
					<td><?=$request->client->name?></td>
					<td><? if($request->source == '1') { ?>Ручной ввод<? } else { ?>Форма на сайте<? } ?></td>
					<td><?=Date::format($request->date,'d F Y')?></td>
					<td><span class="label label-success"><?=$statuses[$request->status]?></span></td>
				</tr>
			<? } ?>
		</tbody>
	</table>
</div>

<?=View::factory('requests/_add')?>
